package ictgradschool.industry.lab14.ex05;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.List;

/**
 * A shape which is capable of loading and rendering images from files / Uris / URLs / etc.
 */
public class ImageShape extends Shape {

    private Image image;

    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height, String fileName) throws MalformedURLException {
        this(x, y, deltaX, deltaY, width, height, new File(fileName).toURI());
    }

    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height, URI uri) throws MalformedURLException {
        this(x, y, deltaX, deltaY, width, height, uri.toURL());
    }

    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height, URL url) {
        super(x, y, deltaX, deltaY, width, height);

        ImageLoader loadImage = new ImageLoader(width, height, url);
        loadImage.execute();

    }

    private class ImageLoader extends SwingWorker<Image, Void> {
        int width;
        int height;
        URL url;

        public ImageLoader(int width, int height, URL url){
            this.width = width;
            this.height = height;
            this.url = url;

        }

        protected Image doInBackground(){
            Image loadimage = null;

            try {
                loadimage = ImageIO.read(url);
                if (width == loadimage.getWidth(null) && height == loadimage.getHeight(null)) {
                    loadimage = loadimage;
                } else {
                    loadimage = loadimage.getScaledInstance(width, height, Image.SCALE_SMOOTH);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return loadimage;
        }

        protected void done(){
            try {
                image = get();
            } catch (Exception exp) {
                exp.printStackTrace();
            }
        }
    }


    @Override
    public void paint(Painter painter) {
        if(image == null){
            painter.setColor(Color.gray);
            painter.fillRect(fX, fY, fWidth, fHeight);
            painter.setColor(Color.red);
            painter.drawCenteredText("Loading...",fX, fY, fWidth, fHeight);

        }else{
            painter.drawImage(this.image, fX, fY, fWidth, fHeight);
        }

    }
}
